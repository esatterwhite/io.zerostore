Node io.ZeroStore
=================

A ZeroMQ backend store for socket.io to enable complex pub/sub behavior. It servers as a replacement for RedisStore, primary use intended for wallace.io
