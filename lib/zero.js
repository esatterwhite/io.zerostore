/*jshint laxcomma:true, smarttabs: true */

/**
 * A ZeroMQ backend store for socket.io to enable complex pub/sub behavior.
 * @module zero
 * @author Eric Satterwhite
 * @requires path
 * @requires zmq
 * @requires util
 * @requires socket.io
 **/
var  path       = require( 'path' )
	,zmq        = require( 'zmq' )
	,util       = require( 'util' )
	,os         = require("os")
	,Store      = require("socket.io").Store
	,basepath
	,defaults
	,ZeroCient
	,ZeroStore

defaults = {
	pub:  "ipc://" + path.join( os.tmpdir(), "zerostore-pub" )
	,sub:  "ipc://" + path.join( os.tmpdir(), "zerostore-sub" )
	,pack:false
}

/**
 * ZMQ Store for socket io
 * @class module:zero.ZeroStore
 * @extends EventEmitter
 * @param {object} options configuration options
 * @param {string} [options.pub=ipc:///tmp/zerostore-pub] 
 * @param {string} [options.sub=ipc:///tmp/zerostore-sub] 
 */
ZeroStore = function( opts ){
	opts = opts || {};

	var options = {};

	for( var key in defaults ){
		options[ key ] = opts[ key ] || defaults[ key ];
	}

	this.options = options;

	this.clients = {};

	this.nodeId = (function () {
		// by default, we generate a random id
		return Math.abs(Math.random() * Math.random() * Date.now() | 0);
	}());

	this.pub = zmq.createSocket( 'pub' );
	this.pub.setsockopt( zmq.ZMQ_SNDHWM, 0);
	// we want to publish to the xsub proxy
	this.pub.connect( this.options.sub );

	this.sub = zmq.createSocket( 'sub' );
	this.sub.setsockopt( zmq.ZMQ_RCVHWM, 0);
	this.sub.connect( this.options.pub ); // subs want messages from the xpublisher

	Store.call( this, opts );

	this.sub.setMaxListeners( 0 );
	this.setMaxListeners( 0 );

}

ZeroStore.prototype.__proto__ = Store.prototype;

/**
 * retrieves a known client by id
 * @method module:zero.ZeroStore#client
 * @param {String} id a socket.io client id
 * @return {Client} matching client if found
 **/
ZeroStore.prototype.client = function( id ){
	if(!this.clients[id]){
		this.clients[id] = new ZeroStore.Client( this, id );
	}

	return this.clients[ id ];
}

/**
 * destroys a client by id aftera specified amount of time
 * @chainable
 * @method module:zero.ZeroStore#destroyClient
 * @param {string} id
 * @param {Number} expiration
 * @return {ZeroStore}
 **/
ZeroStore.prototype.destroyClient = function( id, expiration ){
	if( this.clients[ id ]){
		this.clients[ id ].destroy( expiration );
		delete this.clients[ id ];
	}

	return this;
}

/**
 * 
 * @chainable
 * @method module:zero.ZeroStore#destroy
 * @param {Number} expiration
 * @return
 **/
ZeroStore.prototype.destroy = function( expiration ){
	var keys
		, count;

	keys = Object.keys( this.clients );
	count = keys.length;

	for( var x =0, l = count; x < l; x++ ){
		this.destroyClient( keys[ x ], expiration );
	}

	this.clients = {};

	this.pub.close();
	this.sub.close();

	return this;

}

/**
 * publishes a named event and data
 * @chainable
 * @method module:zero.ZeroStore#publish
 * @param {String} name
 * @param {...object} any number arguments to be passed as data
 * @return ZeroStore
 **/
ZeroStore.prototype.publish = function(name){
	var args = Array.prototype.slice.call(arguments, 1);
	// info("publishing to %s", name)
	this.pub.send( this.pack(name, {nodeId: this.nodeId, name: name, args: args }) )
	this.emit.apply( this, ['publish', name ].concat( args ));
	return this;
}

/**
 * subscribe to an event by name
 * @chainable
 * @method module:zero.ZeroStore#subscribe
 * @param {String} name name of the event
 * @param {function} event handler
 * @return ZeroStore
 **/
ZeroStore.prototype.subscribe = function(name, consumer, fn ){
	var message
	   ,self
	   ,subscribe;

	self = this;
	var unsubscribe = function( ch ){
		if( name == ch ){
			self.sub.removeListener( 'message', message);
			self.removeListener( 'unsubscribe', unsubscribe);
		}
	}
	if( consumer || fn ){
		var self = this;
		this.sub.setsockopt( zmq.ZMQ_SUBSCRIBE, new Buffer(name) );

		message = function( msg ){
			var _name, _evt;

			msg = msg && msg.toString("utf8");
			_name = name;
			_evt = msg.split("**")[0];
			msg = self.unpack( msg );

			if(_name !== _evt ){
				return;
			}

			if( self.nodeId != msg.nodeId ){
				consumer.apply(null, msg.args );
			}
		}

		this.sub.on( "message", message );
		self.on('unsubscribe', unsubscribe );

	}
	this.emit('subscribe', name, consumer, fn );
	return this;
}

/**
 * DESCRIPTION
 * @method module:zero.ZeroStore#unsubscribe
 * @param {TYPE} NAME ...
 * @param {TYPE} NAME ...
 * @return
 **/
ZeroStore.prototype.unsubscribe = function(name, fn ){
	this.sub.unsubscribe( name );

	if( !fn ){ return; }

	var client = this.sub;
	this.sub.setsockopt(zmq.ZMQ_UNSUBSCRIBE, Buffer( name ) )
	client.on( 'unsubscribe', function unsubscribe( ch ){
		if( name == ch ){
			fn()
			client.removeListeners('unsubscribe', unsubscribe );
		}
	})

	this.emit('unsubscribe', name, fn );
}

/**
 * packages a message suitable to be caught by zmq as an event
 * @method module:zero.ZeroStore#pack
 * @param {String} name the name of event for zmq/socket.io
 * @param {Object} Object containg data for socket.io
 * @return the package message for sending over socket.io
 **/
ZeroStore.prototype.pack = function( name, msg ){
	return  name + "**" + JSON.stringify( msg );
}

/**
 * unpacks an incoming message from socket.io
 * @private
 * @method module:zero.ZeroStore#unpack
 * @param {String} msg
 * @return Object
 **/
ZeroStore.prototype.unpack = function( msg ){
	var bits = msg.split("**");
	return JSON.parse( bits[1] );
}


/**
 * Internal client for socket.io and stores
 * @class module:zero.ZeroClient
 * @extends EventEmitter
 * @param {ZeroStore} store the store this client belongs to
 * @param {string} id internal id for the store
 * @example var x = new NAME.Thing({});
 */
ZeroClient = function( store, id ){
	this.store = store;
	this.id = id
}
/**
 * Destrothe interal data set
 * @param {TYPE} expire time to expire in seconds
 **/
ZeroClient.prototype.destroy = function( exp ){
	if( typeof exp != 'number'){
		this.data = {};
	} else{
		setTimeout( function(){
			this.data={}
		}.bind( this ), exp * 1000 )
	}
}

// un supports client methods
// here so it doesn't die
ZeroClient.prototype.get = function(){
	console.log("ATTEMPTED GET ", arguments );
}

ZeroClient.prototype.set = function(){
	console.log("ATTEMPTED SET ", arguments );
}


ZeroClient.prototype.has = function(){

}

ZeroClient.prototype.del = function( ey, fn){
	fn(null, null)
}




// attatch the client to the main store
// as convienence
ZeroStore.Client = ZeroClient;

// export the Store
module.exports = ZeroStore;
